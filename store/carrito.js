export const state = () => ({
    list: []
  })
  
  export const mutations = {
    add(state, text) {
      state.list.push({
        text,
        done: false
      })
    },
    remove(state,  productId ) {
      state.list.filter(o => o).find((obj,i) => {
        console.log(obj.text.id, productId );
        if(obj.text.id === productId){
          state.list.splice(i, 1);
        }
      });
    },
    toggle(carrito) {
        carrito.done = !carrito.done
    }
  }
  export const getters = {
    getCarrito (state) {
      return state.list
    },
    getCarritoById: (state) => (id) => {
      return state.list.find(todo => todo.text.id === id)
    }
  }