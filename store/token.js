export const state = () => ({
    list: []
  })
  
  export const mutations = {
    add(state, text) {
      state.list = text;
    },
    remove(state) {
        state.list = "";
    },
    toggle(token) {
        token.done = !token.done
    }
  }
  export const getters = {
    getToken (state) {
      if(state.list){
        return true;
      }else{
        return false;
      }
    }
    
  }