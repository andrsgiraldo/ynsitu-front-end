import axios from 'axios'

export default axios.create({
  ApiUrl: process.env.ApiUrl
})